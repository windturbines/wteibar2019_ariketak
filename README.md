# WTEIBAR2019_Ariketak

This repository contains the exercises and the data that we will be using during the workshop in Eibar.

Los datos que utilizaremos se encuentran en la carpeta Datos. 
La carpeta Ejercicios contiene los scripts que utilizaremos. Aseguraos de haber instalado los paquetes que se cargan en las primeras filas del script ejercicios.Rmd.

Si tenéis cualquier problema, contactad conmigo.
