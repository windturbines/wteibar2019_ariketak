


create_bins <- function(X, bins = NA, by = NA,  start_from = NA, end_in = NA){
  
  output <- vector(length = length(X))
  
  if(is.na(start_from)){
    
    start_from <- floor(min(X))
  }
  
  if(is.na(end_in)){
    
    end_in <- ceiling(max(X))
  }
  
  stopifnot(!is.na(bins)| !is.na(by) )
  
  if(!is.na(bins) & !is.na(by)){
    
    message('The value of bins will be used when both bins and by are given.')
    
    by <- (end_in - start_from) / bins
    
    
  }else if(!is.na(bins)){
    
    by <- (end_in - start_from) / bins
    
  }
  

  bin_limits <- seq(from = start_from, to = end_in, by = by)
  
  bin_position <- vector(length = (length( bin_limits)-1))
  
  for(position in seq_along(bin_position) ){
    
    bin_position[position] <- (bin_limits[position+1]-bin_limits[position]) / 2 + bin_limits[position]
      
  }
  
  
  for(bin_n in seq_along(bin_limits)[-length(bin_limits)] ){
    
    idx <- X>=bin_limits[bin_n] &  X<=bin_limits[bin_n+1]
    
    output[idx] <- bin_position[bin_n]
    

  }
  
  return(output)
}




aniade_nombre <- function(df, varname){
  
  df$Bin_center <- as.numeric(varname)
  df
  
}


calcula_descriptores_bin <- function(wt, variable, filtrar_estado = NA) {
  
  if(!is.na(filtrar_estado)){ 
  wt <- wt[wt$Estado %in% filtrar_estado,]
  }
  variable_estudio <- wt[, variable]
  variable_grupo <- wt[,'Bin']
  
  summary_bin <- tapply(variable_estudio, INDEX = variable_grupo , FUN = descriptores, simplify = FALSE)
  
  summary_bin <- mapply(summary_bin, names(summary_bin), FUN = aniade_nombre, SIMPLIFY = FALSE)
  
  summary_bin <- do.call('rbind', summary_bin)
  
}
